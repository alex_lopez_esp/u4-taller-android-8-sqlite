package dam.android.alex.u4t8database;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dam.android.alex.u4t8database.data.TodoListDBManager;
import dam.android.alex.u4t8database.model.Task;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private TodoListDBManager todoListDBManager;
    private ArrayList<Task> myTaskList;


    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvId;
        TextView tvTodo;
        TextView tvToAccomplish;
        TextView tvDescription;
        TextView tvPriority;
        TextView tvStatus;

        public MyViewHolder(View view) {
            super(view);
            this.tvId = view.findViewById(R.id.tvId);
            this.tvTodo = view.findViewById(R.id.tvTodo);
            this.tvToAccomplish = view.findViewById(R.id.tvToAccomplish);
            this.tvDescription = view.findViewById(R.id.tvDescription);
            this.tvPriority=view.findViewById(R.id.priority);
            this.tvStatus=view.findViewById(R.id.status);
        }
        public void bind(Task task){
            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvTodo.setText(task.getTodo());
            this.tvToAccomplish.setText(task.getToAccomplish());
            this.tvDescription.setText(task.getDescription());
            this.tvPriority.setText(task.getPriority());
            this.tvStatus.setText(task.getStatus());

        }
    }
    public MyAdapter(TodoListDBManager todoListDBManager){
        this.todoListDBManager=todoListDBManager;

    }

    public void getData(){
        this.myTaskList= todoListDBManager.getTasks();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout= LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false);
        return new MyViewHolder(itemLayout);
    }
    @Override
    public void onBindViewHolder(MyViewHolder viewHolder,int position){
        viewHolder.bind(myTaskList.get(position));
    }
    @Override
    public int getItemCount(){
        return myTaskList.size();
    }

}
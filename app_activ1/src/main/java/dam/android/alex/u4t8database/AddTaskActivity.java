package dam.android.alex.u4t8database;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import dam.android.alex.u4t8database.data.TodoListDBManager;

public class AddTaskActivity extends AppCompatActivity {
    private EditText etTodo;
    private EditText etToAccomplish;
    private EditText etDescription;
    private Spinner spinnerStatus;
    private Spinner spinnerPriority;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        setUI();
    }
    private void setUI(){
        etTodo=findViewById(R.id.etTodo);
        etToAccomplish=findViewById(R.id.etToAccomplish);
        etDescription=findViewById(R.id.etDescription);
        spinnerStatus=findViewById(R.id.spinnerStatus);
        spinnerPriority=findViewById(R.id.spinnerPriority);

        ArrayAdapter<CharSequence> spinnerAdapterStatus=ArrayAdapter.createFromResource(this,R.array.status, android.R.layout.simple_spinner_item);
        spinnerAdapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(spinnerAdapterStatus);

        ArrayAdapter<CharSequence> spinnerAdapterPriority=ArrayAdapter.createFromResource(this,R.array.priority, android.R.layout.simple_spinner_item);
        spinnerAdapterPriority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPriority.setAdapter(spinnerAdapterPriority);



    }
    public void onClick(View view){
        if (view.getId()==R.id.buttonOK){
            if (etTodo.getText().toString().length()>0){
                TodoListDBManager todoListDBManager=new TodoListDBManager(this);
                Resources rs=getResources();
                String[] status=rs.getStringArray(R.array.status);
                String[] priority=rs.getStringArray(R.array.priority);

                todoListDBManager.insert(etTodo.getText().toString(),etToAccomplish.getText().toString(),etDescription.getText().toString(),
                       status[spinnerStatus.getSelectedItemPosition()].toString(), priority[spinnerPriority.getSelectedItemPosition()].toString());
            }else {
                Toast.makeText(this,"Task name is empty",Toast.LENGTH_LONG).show();
            }
            finish();
        }
    }
}
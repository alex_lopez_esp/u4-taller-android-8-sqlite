package dam.android.alex.u4t8database.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import dam.android.alex.u4t8database.model.Task;

public class TodoListDBManager {
    private  TodoListDBHelper todoListDBHelper;

    public TodoListDBManager(Context context){
        todoListDBHelper=TodoListDBHelper.getInstance(context);
    }

    public void insert(String todo,String when,String description,String status,String priority){

        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase !=null){
            ContentValues contentValues=new ContentValues();
            contentValues.put(TodoListDBContact.Tasks.TODO,todo);
            contentValues.put(TodoListDBContact.Tasks.TO_ACCOMPLISH,when);
            contentValues.put(TodoListDBContact.Tasks.DESCRIPTION,description);
            contentValues.put(TodoListDBContact.Tasks.STATUS,status);
            contentValues.put(TodoListDBContact.Tasks.PRIORITY,priority);

            sqLiteDatabase.insert(TodoListDBContact.Tasks.TABLE_NAME,null,contentValues);
        }

    }
    public void update(int id,String todo,String when,String description,String status,String priority){//TODO Consulta para actualizar la informacion de la BD
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();

            if (sqLiteDatabase!=null){
                ContentValues contentValues=new ContentValues();
                contentValues.put(TodoListDBContact.Tasks._ID,id);
                contentValues.put(TodoListDBContact.Tasks.TODO,todo);
                contentValues.put(TodoListDBContact.Tasks.TO_ACCOMPLISH,when);
                contentValues.put(TodoListDBContact.Tasks.DESCRIPTION,description);
                contentValues.put(TodoListDBContact.Tasks.STATUS,status);
                contentValues.put(TodoListDBContact.Tasks.PRIORITY,priority);
                sqLiteDatabase.update(TodoListDBContact.Tasks.TABLE_NAME,contentValues,"_id = "+id,null);
            }

    }
    public void delete(int id){//TODO Consulta para borrar por id
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase!=null){
            ContentValues contentValues=new ContentValues();
            contentValues.put(TodoListDBContact.Tasks.TODO,id);
            sqLiteDatabase.delete(TodoListDBContact.Tasks.TABLE_NAME,"_id = "+id,null);
        }
    }
    public void deleteCompleted(){//TODO Consulta para borrar los que tienen los status en completed
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase!=null){
            String[] completed=new String[]{"Completed"};
            sqLiteDatabase.delete(TodoListDBContact.Tasks.TABLE_NAME,TodoListDBContact.Tasks.STATUS+" like ?",completed);
        }
    }


    public void deleteAll(){//TODO Consulta para borrar todos los datos
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase!=null){
            sqLiteDatabase.delete(TodoListDBContact.Tasks.TABLE_NAME,null,null);

        }

    }


    public ArrayList<Task> showbynot_started(){//TODO Consulta para obtener los datos necesarios
        ArrayList<Task> arrayList=new ArrayList<>();
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase!=null){
           String[] completed=new String[]{"Not Started"};

            Cursor cursor=sqLiteDatabase.query(TodoListDBContact.Tasks.TABLE_NAME,null,TodoListDBContact.Tasks.STATUS+" = ?",completed,null,null,null);
            if (cursor!=null){
                int _idIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks._ID);
                int todoIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.TODO);
                int to_AccomplishIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.TO_ACCOMPLISH);
                int descriptionIndex= cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.DESCRIPTION);
                int status=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.STATUS);
                int priority= cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.PRIORITY);

                while (cursor.moveToNext()){
                    Task task=new Task(
                            cursor.getInt(_idIndex),
                            cursor.getString(todoIndex),
                            cursor.getString(to_AccomplishIndex),
                            cursor.getString(descriptionIndex),
                            cursor.getString(status),
                            cursor.getString(priority));

                    arrayList.add(task);
                }
                cursor.close();

            }


        }
        return arrayList;

    }


    public ArrayList<Task> showbyProgress() {//TODO Consulta para obtener los datos necesarios
        ArrayList<Task> arrayList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            String[] completed = new String[]{"In Progress"};

            Cursor cursor=sqLiteDatabase.query(TodoListDBContact.Tasks.TABLE_NAME,null,TodoListDBContact.Tasks.STATUS+" = ?",completed,null,null,null);
            if (cursor!=null){
                int _idIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks._ID);
                int todoIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.TODO);
                int to_AccomplishIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.TO_ACCOMPLISH);
                int descriptionIndex= cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.DESCRIPTION);
                int status=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.STATUS);
                int priority= cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.PRIORITY);

                while (cursor.moveToNext()){
                    Task task=new Task(
                            cursor.getInt(_idIndex),
                            cursor.getString(todoIndex),
                            cursor.getString(to_AccomplishIndex),
                            cursor.getString(descriptionIndex),
                            cursor.getString(status),
                            cursor.getString(priority));

                    arrayList.add(task);
                }
                cursor.close();

            }


        }
        return arrayList;

    }

    @SuppressLint("Recycle")
    public ArrayList<Task> showbyCompleted() {//TODO Consulta para obtener los datos necesarios
        ArrayList<Task> arrayList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase!=null){
            String[] completed=new String[]{"Completed"};

            Cursor cursor=sqLiteDatabase.query(TodoListDBContact.Tasks.TABLE_NAME,null,TodoListDBContact.Tasks.STATUS+" = ?",completed,null,null,null);
            if (cursor!=null){
                int _idIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks._ID);
                int todoIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.TODO);
                int to_AccomplishIndex=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.TO_ACCOMPLISH);
                int descriptionIndex= cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.DESCRIPTION);
                int status=cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.STATUS);
                int priority= cursor.getColumnIndexOrThrow(TodoListDBContact.Tasks.PRIORITY);

                while (cursor.moveToNext()){
                    Task task=new Task(
                            cursor.getInt(_idIndex),
                            cursor.getString(todoIndex),
                            cursor.getString(to_AccomplishIndex),
                            cursor.getString(descriptionIndex),
                            cursor.getString(status),
                            cursor.getString(priority));

                    arrayList.add(task);
                }
                cursor.close();

            }


        }
        return arrayList;

    }





    public ArrayList<Task> getTasks(){

        ArrayList<Task> tasksList=new ArrayList<>();
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase!=null){
            String[] projection=new String[]{TodoListDBContact.Tasks._ID,
                    TodoListDBContact.Tasks.TODO,
                    TodoListDBContact.Tasks.TO_ACCOMPLISH,
                    TodoListDBContact.Tasks.DESCRIPTION,
                    TodoListDBContact.Tasks.STATUS,
                    TodoListDBContact.Tasks.PRIORITY};

            Cursor cursorTodoList= sqLiteDatabase.query(TodoListDBContact.Tasks.TABLE_NAME,
                    projection,null,null,null,null,null,null);

            if (cursorTodoList!=null){
                int _idIndex=cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks._ID);
                int todoIndex=cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.TODO);
                int to_AccomplishIndex=cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.TO_ACCOMPLISH);
                int descriptionIndex= cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.DESCRIPTION);
                int status=cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.STATUS);
                int priority= cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.PRIORITY);

                while (cursorTodoList.moveToNext()){
                    Task task=new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(status),
                            cursorTodoList.getString(priority));

                    tasksList.add(task);
                }
                cursorTodoList.close();

            }
        }
        return tasksList;

    }
    public void close(){
        todoListDBHelper.close();
    }


}

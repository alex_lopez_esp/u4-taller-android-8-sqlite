package dam.android.alex.u4t8database;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import dam.android.alex.u4t8database.data.TodoListDBContact;
import dam.android.alex.u4t8database.data.TodoListDBManager;
import dam.android.alex.u4t8database.model.Task;

public class MainActivity extends AppCompatActivity implements MyAdapter.onItemClickListener {
    private RecyclerView rvTodoList;
    private TodoListDBManager todoListDBManager;
    private MyAdapter myAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        todoListDBManager=new TodoListDBManager(this);
        myAdapter=new MyAdapter(todoListDBManager,this);
        setUI();
    }

    private void setUI() {
        setSupportActionBar(findViewById(R.id.toolbar));

        FloatingActionButton fab=findViewById(R.id.fab);
        fab.setOnClickListener((view)->{
            startActivity(new Intent(getApplicationContext(),AddTaskActivity.class));
        });
        rvTodoList=findViewById(R.id.rvTodoList);
        rvTodoList.setHasFixedSize(true);
        rvTodoList.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        rvTodoList.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        rvTodoList.setAdapter(myAdapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        myAdapter.getData();
    }

    @Override
    protected void onDestroy() {
        todoListDBManager.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {//TODO APP_ACTIV3 Lo que hacemos aqui es que las seleccion que accemos en el bar mostramos /borramos
        //Todo los datos de la BD


        switch (item.getItemId()){
            case R.id.delete_completed:
                todoListDBManager.deleteCompleted();
                myAdapter.getData();
                break;


            case R.id.delete_all:
                todoListDBManager.deleteAll();
                myAdapter.getData();
                break;

            case R.id.not_started:
                myAdapter.introducirArray(todoListDBManager.showbynot_started());

                break;


            case R.id.in_progress:
                myAdapter.introducirArray(todoListDBManager.showbyProgress());

                break;

            case R.id.completed:
                myAdapter.introducirArray(todoListDBManager.showbyCompleted());

                break;

            case R.id.all:
                myAdapter.getData();

                break;



        }
        return super.onOptionsItemSelected(item);

    }




    @Override
    public void ItemClickListener(Task task) {
        Bundle bundle=new Bundle();

        bundle.putSerializable("task",task);

        Intent intent=new Intent(this,Edit_Task_Activity.class);
        intent.putExtras(bundle);
        startActivity(intent);


    }
}
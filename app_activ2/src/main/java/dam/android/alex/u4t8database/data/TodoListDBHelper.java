package dam.android.alex.u4t8database.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TodoListDBHelper extends SQLiteOpenHelper {
    private static  TodoListDBHelper instanceDBHelper;

    public TodoListDBHelper(Context context){
        super(context,TodoListDBContact.DB_NAME,null,TodoListDBContact.DB_VERSION);
    }

    public static synchronized TodoListDBHelper getInstance(Context context){
        if (instanceDBHelper==null){
            instanceDBHelper=new TodoListDBHelper(context.getApplicationContext());
        }
        return instanceDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TodoListDBContact.Tasks.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(TodoListDBContact.Tasks.DELETE_TABLE);
        onCreate(sqLiteDatabase);
    }


}

package dam.android.alex.u4t8database;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.service.autofill.OnClickAction;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import dam.android.alex.u4t8database.data.TodoListDBManager;
import dam.android.alex.u4t8database.model.Task;

public class Edit_Task_Activity extends AppCompatActivity implements View.OnClickListener {
    private EditText etTodo;
    private EditText etToAccomplish;
    private EditText etDescription;
    private Spinner spinnerStatus;
    private Spinner spinnerPriority;
    private int id;
    private Button delete;
    private Button save;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);
        setUI();
    }
    public void setUI(){
        etTodo=findViewById(R.id.etTodo2);
        etToAccomplish=findViewById(R.id.etToAccomplish2);
        etDescription=findViewById(R.id.etDescription2);
        spinnerStatus=findViewById(R.id.spinnerPriority4);
        spinnerPriority=findViewById(R.id.spinnerStatus2);
        ArrayAdapter<CharSequence> spinnerAdapterStatus=ArrayAdapter.createFromResource(this,R.array.status, android.R.layout.simple_spinner_item);
        spinnerAdapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(spinnerAdapterStatus);

        ArrayAdapter<CharSequence> spinnerAdapterPriority=ArrayAdapter.createFromResource(this,R.array.priority, android.R.layout.simple_spinner_item);
        spinnerAdapterPriority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPriority.setAdapter(spinnerAdapterPriority);

        ponerdatos();

    }
    public void ponerdatos(){
       /* Bundle bundle=getIntent().getExtras();
        Task task=(Task) bundle.getSerializable("task");*/

        Task task=(Task) getIntent().getSerializableExtra("task");


        this.etTodo.setText(task.getTodo());
        this.etToAccomplish.setText(task.getToAccomplish());
        this.etDescription.setText(task.getDescription());
        System.out.println(task.getStatus());

        switch (task.getStatus()){
            case " Not Started":
                this.spinnerStatus.setSelection(0);
                break;
            case " In Progress":
                this.spinnerStatus.setSelection(1);
                break;
            case " Completed":
                this.spinnerStatus.setSelection(2);
                break;
        }

        switch (task.getPriority()){
            case " Low":
                this.spinnerPriority.setSelection(0);
                break;
            case " Normal":
                this.spinnerPriority.setSelection(1);
                break;
            case " High":
                this.spinnerPriority.setSelection(2);
                break;
        }


        this.delete=findViewById(R.id.delete);
        this.save=findViewById(R.id.save);
        save.setOnClickListener(this::onClick);
        delete.setOnClickListener(this::onClick);
        id=task.get_id();




    }


    @Override
    public void onClick(View v) {
        TodoListDBManager todoListDBManager=new TodoListDBManager(this);
        if (v.getId()==R.id.save){

            if (etTodo.getText().toString().length()>0) {

                Resources rs = getResources();
                String[] status = rs.getStringArray(R.array.status);
                String[] priority = rs.getStringArray(R.array.priority);

                todoListDBManager.update(id, etTodo.getText().toString(), etToAccomplish.getText().toString(), etDescription.getText().toString(),
                        status[spinnerStatus.getSelectedItemPosition()].toString(), priority[spinnerPriority.getSelectedItemPosition()].toString());


            }
            }else if (v.getId()==R.id.delete){
                todoListDBManager.delete(id);

            }
        finish();








    }
}
package dam.android.alex.u4t8database;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toolbar;

import dam.android.alex.u4t8database.data.TodoListDBManager;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvTodoList;
    private TodoListDBManager todoListDBManager;
    private MyAdapter myAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        todoListDBManager=new TodoListDBManager(this);
        myAdapter=new MyAdapter(todoListDBManager);
        setUI();
    }

    private void setUI() {

        setSupportActionBar(findViewById(R.id.toolbar));

        FloatingActionButton fab=findViewById(R.id.fab);
        fab.setOnClickListener((view)->{
            startActivity(new Intent(getApplicationContext(),AddTaskActivity.class));
        });
        rvTodoList=findViewById(R.id.rvTodoList);
        rvTodoList.setHasFixedSize(true);
        rvTodoList.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        rvTodoList.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        rvTodoList.setAdapter(myAdapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        myAdapter.getData();
    }

    @Override
    protected void onDestroy() {
        todoListDBManager.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
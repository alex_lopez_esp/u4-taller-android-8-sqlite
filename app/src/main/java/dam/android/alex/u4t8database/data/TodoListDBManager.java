package dam.android.alex.u4t8database.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import dam.android.alex.u4t8database.model.Task;

public class TodoListDBManager {
    private  TodoListDBHelper todoListDBHelper;
    public TodoListDBManager(Context context){
        todoListDBHelper=TodoListDBHelper.getInstance(context);
    }

    public void insert(String todo,String when,String description){

        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase !=null){
            ContentValues contentValues=new ContentValues();
            contentValues.put(TodoListDBContact.Tasks.TODO,todo);
            contentValues.put(TodoListDBContact.Tasks.TO_ACCOMPLISH,when);
            contentValues.put(TodoListDBContact.Tasks.DESCRIPTION,description);

            sqLiteDatabase.insert(TodoListDBContact.Tasks.TABLE_NAME,null,contentValues);
        }

    }
    public ArrayList<Task> getTasks(){

        ArrayList<Task> tasksList=new ArrayList<>();
        SQLiteDatabase sqLiteDatabase=todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase!=null){
            String[] projection=new String[]{TodoListDBContact.Tasks._ID,
                    TodoListDBContact.Tasks.TODO,
                    TodoListDBContact.Tasks.TO_ACCOMPLISH,
                    TodoListDBContact.Tasks.DESCRIPTION};

            Cursor cursorTodoList= sqLiteDatabase.query(TodoListDBContact.Tasks.TABLE_NAME,
                    projection,null,null,null,null,null);

            if (cursorTodoList!=null){
                int _idIndex=cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks._ID);
                int todoIndex=cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.TODO);
                int to_AccomplishIndex=cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.TO_ACCOMPLISH);
                int descriptionIndex= cursorTodoList.getColumnIndexOrThrow(TodoListDBContact.Tasks.DESCRIPTION);

                while (cursorTodoList.moveToNext()){
                    Task task=new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex));
                    tasksList.add(task);
                }
                cursorTodoList.close();

            }
        }
        return tasksList;

    }
    public void close(){
        todoListDBHelper.close();
    }
}
